<?php
/**
 * @author: StefanHelmer
 */

class SimpleCryptTest extends \PHPUnit\Framework\TestCase {

    private $test_value = 'Simple Crypt Unit Test';
    private $test_secret = 'dlfksdlfkdslfkldskf';

    /**
     * @throws \Rockschtar\SimpleCrypt\Exceptions\SimpleCryptException
     */
    public function testNoBrainer(): void {
        $simple_crypt = new \Rockschtar\SimpleCrypt\SimpleCrypt(md5('dlfksdlfkdslfkldskf'));
        $encrypted_value = $simple_crypt->encrypt($this->test_value);
        $decrypted_value = $simple_crypt->decrypt($encrypted_value);
        $this->assertEquals($this->test_value, $decrypted_value);
    }

    /**
     * @throws \Rockschtar\SimpleCrypt\Exceptions\SimpleCryptException
     */
    public function testWithCustomIV(): void {
        $simple_crypt = new \Rockschtar\SimpleCrypt\SimpleCrypt(md5('dlfksdlfkdslfkldskf'), random_bytes(16));
        $encrypted_value = $simple_crypt->encrypt($this->test_value);
        $decrypted_value = $simple_crypt->decrypt($encrypted_value);
        $this->assertEquals($this->test_value, $decrypted_value);
    }

}