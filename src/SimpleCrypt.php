<?php
/**
 * @author: StefanHelmer
 */

namespace Rockschtar\SimpleCrypt;

use Rockschtar\SimpleCrypt\Exceptions\SimpleCryptException;
use Rockschtar\SimpleCrypt\Models\Action;
use Rockschtar\SimpleCrypt\Models\Options;

class SimpleCrypt {

    private $less_secure = false;

    private $key;

    private $iv;

    /**
     * SimpleCrypt constructor.
     * @param string $key
     * @param string|null $iv
     * @throws SimpleCryptException
     */
    public function __construct(string $key, ?string $iv = null) {

        if(empty($key)) {
            throw new SimpleCryptException('No secret key is set');
        }

        if(mb_strlen($key, '8bit') !== 32) {
            throw new SimpleCryptException('Key should be 256-Bit length');
        }

        if(null === $iv || $iv === '') {
            $this->less_secure = true;
            /** @noinspection PhpUnhandledExceptionInspection */
            $iv = random_bytes(16);
        }

        $this->key = $key;
        $this->iv = $iv;
    }

    /**
     * @param string $value
     * @return string
     */
    public function encrypt(string $value): string {
        return $this->crypt($value, Action::ENCRYPT);
    }

    /**
     * @param string $value
     * @param string $action
     * @return string
     */
    private function crypt(string $value, string $action = Action::ENCRYPT): string {

        $output = false;
        $encrypt_method = 'AES-256-CBC';
        $key = hash('sha256', $this->key);
        $iv = $this->iv;

        if($this->less_secure) {
            if($action === Action::ENCRYPT) {
                /** @noinspection EncryptionInitializationVectorRandomnessInspection */
                $output = base64_encode($iv . openssl_encrypt($value, $encrypt_method, $key, 0, $iv));
            } else if($action === Action::DECRYPT) {
                $iv = substr(base64_decode($value), 0, 16);
                $real_value = substr(base64_decode($value), 16, \strlen(base64_decode($value)));
                $output = openssl_decrypt($real_value, $encrypt_method, $key, 0, $iv);
            }
        } else if($action === Action::ENCRYPT) {
            /** @noinspection EncryptionInitializationVectorRandomnessInspection */
            $output = base64_encode(openssl_encrypt($value, $encrypt_method, $key, 0, $iv));
        } else if($action === Action::DECRYPT) {
            $output = openssl_decrypt(base64_decode($value), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    /**
     * @param string $value
     * @return string
     */
    public function decrypt(string $value): string {
        return $this->crypt($value, Action::DECRYPT);
    }

}