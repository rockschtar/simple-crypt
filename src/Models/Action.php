<?php
/**
 * @author: StefanHelmer
 */

namespace Rockschtar\SimpleCrypt\Models;

class Action {

    public const ENCRYPT = 'encrypt';
    public const DECRYPT = 'decrypt';
}